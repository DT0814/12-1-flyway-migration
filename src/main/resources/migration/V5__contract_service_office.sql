create table contract_service_offices (
                                          id int not null,
                                          office_id int,
                                          contract_id int,
                                          primary key (id),
                                          foreign key (office_id) references offices(id),
                                          foreign key (contract_id) references contracts(id)
)
