create table offices (
                         id int not null,
                         country varchar(128),
                         city varchar(128),
                         primary key (id)
);

create table staff (
                       id int not null,
                       first_name varchar (128),
                       last_name varchar (128),
                       office_id int,
                       primary key (id),
                       foreign key (office_id) references offices (id)
)
