CREATE TABLE clients (
                         id int not null,
                         full_name varchar(128),
                         abbreviation varchar(6),
                         type varchar(128),
                         primary key (id)
)
