CREATE TABLE contracts (
                           id int not null,
                           name varchar(128),
                           client_id int,
                           primary key (id),
                           foreign key (client_id) references clients(id)
)
